Rails.application.routes.draw do
  get 'sessions/new'
  root 'sessions#new' # define a página de login como a raiz
  get 'home/index'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  post 'auth/get_token_erp_web', to: 'auth#get_token_erp_web'
  post 'auth/get_token', to: 'auth#get_token'

end
