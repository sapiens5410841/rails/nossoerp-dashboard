
import "jquery"; // this import first
import "script"; // then your other imports that use `$`
import "chartkick"
import "Chart.bundle"
import "chartkick/chart.js"

// NOTE: don't use relative imports: `import "./script"`
//       add `pin "script"` to `importmap.rb`

//= require jquery.mask
// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
//import './add_jquery'
import "@hotwired/turbo-rails"
import "controllers"
  