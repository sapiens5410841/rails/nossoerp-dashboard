# app/services/auth_service.rb
=begin
class AuthService
  include HTTParty

  def self.get_token_erp_web(cnpj, username, password)
    # Aqui vai a lógica para pegar o token, por exemplo:
    url = "https://api.example.com/token"  # Substitua pela URL real da API
    headers = { 'Content-Type' => 'application/x-www-form-urlencoded' }
    body = {
      grant_type: 'password',
      username: username,
      password: password
    }

    response = HTTParty.post(url, body: URI.encode_www_form(body), headers: headers)
    response.parsed_response  # Retorna a resposta da API
  end
end
=end

# app/services/api_service.rb
require 'httparty'

class ApiService
  include HTTParty
  base_uri ENV['API_BASE_URL']

  def self.authenticate(cnpj, username, password)
    post('/api/web/token', body: { username: username, password: password, cnpj: cnpj }.to_json, headers: { 'Content-Type' => 'application/json' })
  end

  def self.fetch_company_data(cnpj)
    get("/consultas/get-dados-cnpj/#{cnpj}")
  end

  def self.fetch_user_data(token)
    get('/dashboardweb', headers: { 'Authorization': "Bearer #{token}" })
  end
end

