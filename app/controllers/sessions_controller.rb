# app/controllers/sessions_controller.rb
class SessionsController < ApplicationController
  def new
  end

  def create
    response = ApiService.authenticate(params[:cnpj], params[:username], params[:password])
    if response.code == 200
      session[:token] = response.parsed_response['access_token']
      session[:cnpj] = params[:cnpj]
      redirect_to root_path
    else
      flash.now[:alert] = "Login failed: #{response.parsed_response['message']}"
      render :new
    end
  end

  def logout
    reset_session
    redirect_to login_path
  end
end
