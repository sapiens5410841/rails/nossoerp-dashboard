class AuthController < ApplicationController
    include HTTParty
  
    def get_token_erp_web
      dados_empresa = params[:dados_empresa]  # assume que você recebe isso como parâmetro
      usuario = params[:usuario]
      senha = params[:senha]
  
      headers = { 'Content-Type' => 'application/x-www-form-urlencoded' }
      body = {
        grant_type: 'password',
        username: usuario,
        password: senha
      }
  
      response = HTTParty.post("#{dados_empresa[:url_interna_api]}/api/web/token",
                               body: URI.encode_www_form(body),
                               headers: headers)
  
      if response.parsed_response['access_token']
        set_dados_empresa(dados_empresa, response.parsed_response['access_token'])
        get_dados_usuario
      else
        render json: { error: "Algo de errado não está certo, tente novamente!" }, status: :bad_request
      end
    end
  
    private
  
    def set_dados_empresa(dados_empresa, access_token)
      # Armazene os dados da empresa e o token conforme necessário
    end
  
    def get_dados_usuario
      # Implemente a lógica para obter dados do usuário
    end
  end
  